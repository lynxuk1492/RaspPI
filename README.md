**Build Script for RaspPI**  
curl -sL https://gitlab.com/lynxuk1492/RaspPI/raw/master/test.sh |bash -

**Resize Disk**    
sudo raspi-config

**Update Firmware**    
sudo wget http://goo.gl/1BOfJ -O /usr/bin/rpi-update && sudo chmod +x /usr/bin/rpi-update
sudo rpi-update

**Updates**    
sudo apt-get update  
sudo apt-get upgrade -y

**Full Update only**  
sudo apt-get dist-upgrade

**Components**  
sudo apt-get install git-core git  
sudo apt-get install apache2 -y


